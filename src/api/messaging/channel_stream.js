import Rx from 'rxjs'
import { saveChannelsToRedux } from '../../actions/auth/tenant_auth'

export const ListenForNewChannels = (socket, store) => {
  const p = new Promise((res, rej) => {
    Rx.Observable.create((observer) => {
      socket.on('new_channel', (channel) => {
        observer.next(channel)
			})
    }).subscribe({
      next: (channel) => {
        store.dispatch(saveChannelsToRedux([channel]))
        socket.emit('subscribe_channel', channel.channel_id)
      },
      error: (err) => console.log(err),
      complete: () => {}
    })
    res()
  })
  return p
}
