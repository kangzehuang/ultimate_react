import Rx from 'rxjs'
import { addMessageToRedux } from '../../actions/messaging/tenant_messaging'

export const EstablishChatRelay = (socket, allChannels, store) => {
  const p = new Promise((res, rej) => {
    Rx.Observable.create((observer) => {
      allChannels.map((channel) => {
        console.log(channel)
        socket.emit('subscribe_channel', channel.channel_id)
      })
      socket.on('message_from_server', (data) => {
        console.log(data)
        observer.next(data)
      })
    }).subscribe({
      next: (msg) => store.dispatch(addMessageToRedux(msg)),
      error: (err) => console.log(err),
      complete: () => {}
    })
    res()
  })
  return p
}
