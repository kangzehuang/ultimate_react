import uuid from 'uuid'
import Cookies from 'js-cookie'

export const GrabTenantIdFromCookie = () => {
  const p = new Promise((res, rej) => {
    const userId = Cookies.get('userId')
		if (userId) {
			res(userId)
		} else {
      const newUserId = Math.floor(Math.random() * 9999999999);
			console.log(`Created new user with id ${newUserId}`)
			Cookies.set('userId', newUserId, { expires: 500 });
			res(newUserId)
		}
  })
  return p
}
