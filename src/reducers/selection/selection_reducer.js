import {
  SELECT_BUILDING,
  SELECT_LANDLORD,
} from '../../actions/action_types'

const INITIAL_STATE = {
	current_building: {
    building_id: 25245921949
  },
  current_landlord: {
    landlord_id: 34952930030
  },
}

export default function (state = INITIAL_STATE, action) {
	switch (action.type) {
		case SELECT_BUILDING:
			return {
				...state,
				current_building: action.payload
			}
    case SELECT_LANDLORD:
			return {
				...state,
				current_landlord: action.payload
			}
		default:
			return {
				...state
			}
	}
}
