import { combineReducers } from 'redux'
import tenantReducer from './tenant/tenant_reducer'
import selectionReducer from './selection/selection_reducer'

const rootReducer = combineReducers({
	tenant: tenantReducer,
	selection: selectionReducer,
})

export default rootReducer
