import {
	SET_TENANT_PROFILE,
	ADD_MY_MESSAGE_TO_REDUX,
	ADD_MESSAGE_TO_REDUX,
	SAVE_CHANNELS_TO_REDUX,
} from '../../actions/action_types'

const INITIAL_STATE = {
	tenant_profile: null,
	all_messages: [],
	socket_channels: []
}

export default function (state = INITIAL_STATE, action) {
	switch (action.type) {
		case SET_TENANT_PROFILE:
			return {
				...state,
				tenant_profile: action.payload
			}
		case ADD_MY_MESSAGE_TO_REDUX:
			return {
				...state,
				all_messages: state.all_messages.concat([action.payload])
			}
		case ADD_MESSAGE_TO_REDUX:
			let duplicate_message = false
			state.all_messages.forEach((msg) => {
				if(msg.message_id === action.payload.message_id){
					duplicate_message = true
				}
			})
			return {
				...state,
				all_messages: duplicate_message ? state.all_messages : state.all_messages.concat([action.payload])
			}
		case SAVE_CHANNELS_TO_REDUX:
			return {
				...state,
				socket_channels: state.socket_channels.concat(action.payload)
			}
		default:
			return {
				...state
			}
	}
}
