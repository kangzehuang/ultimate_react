import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import {
  browserHistory,
  Router,
  Route,
} from 'react-router'
import Store from './store'

import AppRoot from './components/AppRoot'
import Dashboard from './components/stats/Dashboard'
import PropertiesList from './components/properties/PropertiesList'
import Chatline from './components/chat/Chatline'

ReactDOM.render(
  <Provider store={Store}>
    <Router history={browserHistory}>
      <Route path='/' component={AppRoot}>
        <Route path='/landlord'>
          <Route path='dashboard' component={Dashboard} />
          <Route path='properties' component={PropertiesList} />
          <Route path='chatline' component={Chatline} />
          <Route path='*' component={Dashboard} />
        </Route>
      </Route>
      <Route path='*' component={AppRoot} />
    </Router>
  </Provider>
  , document.getElementById('root'))
