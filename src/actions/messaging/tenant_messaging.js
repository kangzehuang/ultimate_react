import {
	ADD_MY_MESSAGE_TO_REDUX,
	ADD_MESSAGE_TO_REDUX,
} from '../action_types'

export const addMyMessageToRedux = (msg) => {
	return (dispatch) => {
		dispatch({
			type: ADD_MY_MESSAGE_TO_REDUX,
			payload: msg
		})
	}
}

// save a message to redux, from the global socket connection
export const addMessageToRedux = (msg) => {
	return {
		type: ADD_MESSAGE_TO_REDUX,
		payload: msg
	}
}
