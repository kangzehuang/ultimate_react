import {
	SET_TENANT_PROFILE,
	CONNECT_WEBSOCKETS,
	SAVE_CHANNELS_TO_REDUX
} from '../action_types'

export const setupWebsockets = (userId) => {
	return (dispatch) => {
		dispatch({
			type: CONNECT_WEBSOCKETS,
			payload: userId
		})
	}
}

export const saveTenantProfile = (userId) => {
	return (dispatch) => {
		dispatch({
			type: SET_TENANT_PROFILE,
			payload: {
        tenant_id: userId
      }
		})
	}
}

// save a message to redux, from the global socket connection
export const saveChannelsToRedux = (channels) => {
	return {
		type: SAVE_CHANNELS_TO_REDUX,
		payload: channels
	}
}
