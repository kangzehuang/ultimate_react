import React, { Component } from 'react';
import { connect } from 'react-redux';
import Radium, { StyleRoot } from 'radium'
import {
  Link
} from 'react-router'
import PropTypes from 'prop-types'
import LandlordApp from './landlord/LandlordApp'
import TenantApp from './tenant/TenantApp'
import ChatContainer from './chat/PopupChat/ChatContainer'


class AppRoot extends Component {

  loadHeader() {
    return (
      <ul style={headerStyles().list}>
        <li><Link to='/'>Search</Link></li>
        <li><Link to='/landlord'>Landlord</Link></li>
      </ul>
		)
  }

	render() {
    const landlordMode = this.props.location.pathname.indexOf('/landlord') === 0
		return (
      <div style={comStyles().main}>
        <StyleRoot>
          {this.loadHeader()}
          {
            landlordMode
            ?
            <LandlordApp style={comStyles().app}>
              {this.props.children}
            </LandlordApp>
            :
            <TenantApp style={comStyles().app}>
              {this.props.children}
            </TenantApp>
          }
          <ChatContainer style={comStyles().chat} />
        </StyleRoot>
      </div>
		)
	}
}

AppRoot.propTypes = {
  children: PropTypes.object,
  location: PropTypes.object
}

AppRoot.defaultProps = {
  children: {},
  location: {}
}

const RadiumHOC = Radium(AppRoot)

function mapStateToProps(state) {
	return {

	}
}

export default connect(mapStateToProps, {})(RadiumHOC)

// =============================

const comStyles = () => {
	return {
    main: {
			width: '100vw',
			height: '100vh',
      display: 'flex',
      flexDirection: 'column'
    },
		app: {
			width: '100%',
      justifySelf: 'stretch',
		},
    chat: {
			position: 'absolute',
			bottom: '5px',
			right: '5px',
    }
	}
}

const headerStyles = () => {
  return {
    list: {
      width: '100%',
			height: '50px',
    }
  }
}
