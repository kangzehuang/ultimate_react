import React, { Component } from 'react';
import { connect } from 'react-redux';
import Radium from 'radium'
import PropTypes from 'prop-types'
import { GrabTenantIdFromCookie } from '../../api/auth/tenant_profile'
import { setupWebsockets, saveTenantProfile, addMessageToRedux } from '../../actions/auth/tenant_auth'


class TenantApp extends Component {

	componentWillMount() {
		GrabTenantIdFromCookie().then((userId) => {
			this.props.saveTenantProfile(userId)
			this.props.setupWebsockets(userId)
		}).catch((err) => {
			console.log(err)
		})
  }

	render() {
		return (
			<div style={comStyles().app}>
				{this.props.children}
			</div>
		)
	}
}

TenantApp.propTypes = {
  children: PropTypes.object,
	saveTenantProfile: PropTypes.func,
	setupWebsockets: PropTypes.func,
	addMessageToRedux: PropTypes.func,
	tenant: PropTypes.object
}

TenantApp.defaultProps = {
  children: {}
}

const RadiumHOC = Radium(TenantApp)

function mapStateToProps(state) {
	return {
		tenant: state.tenant.tenant_profile
	}
}

export default connect(mapStateToProps, {
	setupWebsockets,
	saveTenantProfile,
	addMessageToRedux
})(RadiumHOC)

// =============================

const comStyles = () => {
	return {
		app: {
			width: '100%',
			height: '100%',
		}
	}
}
