import React, { Component } from 'react'
import { connect } from 'react-redux'
import Radium from 'radium'
import PropTypes from 'prop-types'
import Chatbox from './Chatbox'
import ChatIcon from './ChatIcon'
import MessageStream from '../../../api/messaging/message_stream'

class ChatContainer extends Component {

	constructor(props) {
		super(props)
		this.state = {
			chat_open: false
		}
	}

	renderChatbox() {
	}

	toggleChatVisible() {
		this.setState({
			chat_open: !this.state.chat_open
		})
	}

	render() {
		return (
			<div style={comStyles().container}>
				{
					this.state.chat_open
					?
					<Chatbox
						id='Chatbox'
						tenant={this.props.tenant}
						toggleChatVisible={() => this.setState({ chat_open: false })}
						style={comStyles().chatbox}
					/>
					:
					null
				}
				<ChatIcon
					id='ChatIcon'
				  toggleChatVisible={() => this.toggleChatVisible()}
				  chat_open={this.state.chat_open}
				  style={comStyles().icon}
				/>
			</div>
		)
	}
}

ChatContainer.propTypes = {
	tenant: PropTypes.object
}

ChatContainer.defaultProps = {
	tenant: null
}

const RadiumHOC = Radium(ChatContainer)

function mapStateToProps(state) {
	return {
		tenant: state.tenant.tenant_profile
	}
}

export default connect(mapStateToProps, {})(RadiumHOC)

// ===============================

const comStyles = () => {
	return {
		container: {
			display: 'flex',
			flexDirection: 'column',
			justifyContent: 'flex-end',
			alignItems: 'flex-end',
			position: 'absolute',
			bottom: '20px',
			right: '20px'
		},
		chatbox: {
		},
		icon: {
		}
	}
}
