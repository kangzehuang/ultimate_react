import React, { Component } from 'react'
import { connect } from 'react-redux'
import Radium from 'radium'
import PropTypes from 'prop-types'
import Rx from 'rxjs'
import uuid from 'uuid'
import {
  xMidBlue
} from '../../../styles/base_colors'
import { addMyMessageToRedux } from '../../../actions/messaging/tenant_messaging'


class Chatbox extends Component {

  constructor() {
    super()
    this.state = {
      inputText: '',
    }
  }

  componentDidMount() {
    // get a hold of our stream sources
    const sendBtn = document.getElementById('sendBtn')
    const textInput = document.getElementById('textInput')

    // create a stream for each source
    const sendBtnStream = Rx.Observable.fromEvent(sendBtn, 'click').map(e => true)
    const enterKeyPressedStream = Rx.Observable.fromEvent(textInput, 'keyup').filter(e => e.keyCode === 13)
    const textEnteredStream = Rx.Observable.fromEvent(textInput, 'keyup').map(e => e.target.value)

    // merge the sendBtnStream and the enterKeyPressedStream
    const sendMessageStream = Rx.Observable.merge(sendBtnStream, enterKeyPressedStream)
    // our merged stream will allow for textEnteredStream until sendMessageStream occurs
    const mergedStream = textEnteredStream.takeUntil(sendMessageStream)

    const onNext = (text) => {
      this.setState({ inputText: text })
    }
    const onError = (err) => {
      console.log(err)
    }
    const onComplete = () => {
      let channelExists = false
      let thisChannelId = `${this.props.tenant.tenant_id}_${this.props.landlord_id}_${this.props.building_id}`
      this.props.socket_channels.forEach((ch) => {
        if(ch.channel_id === thisChannelId){
          channelExists = true
        }
      })
      const newMessage = {
        message_id: uuid.v4(),
        sender_id: this.props.tenant.tenant_id,
        receiver_id: this.props.landlord_id,
        building_id: this.props.building_id,
        channel_id: thisChannelId,
        contents: this.state.inputText,
        channel_exists: channelExists
      }
      textInput.focus()
      this.resizeChat({ target: { value: '' } })
      this.props.addMyMessageToRedux(newMessage)
      mergedStream.subscribe({
        next: onNext,
        error: onError,
        complete: onComplete
      })
    }

    mergedStream.subscribe({
      next: onNext,
      error: onError,
      complete: onComplete
    })
  }

  resizeChat(event) {
    const textareaSize = document.getElementById('textHeight')
    textareaSize.innerHTML = `${event.target.value}\n`
    this.setState({
      inputText: event.target.value
    })
  }

	render() {
		return (
			<div style={comStyles().container}>
        <div style={headerStyles().header}>
          <p style={headerStyles().recipient_name}>Atlas Housing</p>
        </div>
        <div style={chatlogStyles().chatlog}>
          {this.props.all_messages.map((msg) => {
            return (
              <div key={msg.message_id} style={chatlogStyles().messageRow}>
                {
                  msg.sender_id === this.props.tenant.tenant_id
                  ?
                  <div style={chatlogStyles().messageMe}>
                    {msg.contents}
                  </div>
                  :
                  <div style={chatlogStyles().messageThem}>
                    {msg.contents}
                  </div>
                }
              </div>
            )
          })}
          <div style={chatlogStyles().status}></div>
        </div>
        <div style={inputStyles().input}>
          <div id='inputDiv' style={inputStyles().inputDiv}>
            <textarea
              id='textInput'
              onChange={(e) => this.resizeChat(e)}
              value={this.state.inputText}
              style={inputStyles().textInput} >
            </textarea>
            <div id='textHeight' style={inputStyles().textHeight}></div>
            <div id='sendBtn' style={inputStyles().sendBtn}>Send</div>
          </div>
        </div>
			</div>
		)
	}
}

Chatbox.propTypes = {
  tenant: PropTypes.object,
  addMyMessageToRedux: PropTypes.func.isRequired,
  all_messages: PropTypes.array.isRequired,
  socket_channels: PropTypes.array.isRequired,
  building_id: PropTypes.number.isRequired,
  landlord_id: PropTypes.number.isRequired,
}

Chatbox.defaultProps = {
  tenant: null,
  socket_channels: []
}

const RadiumHOC = Radium(Chatbox)

function mapStateToProps(state) {
	return {
    tenant: state.tenant.tenant_profile,
    all_messages: state.tenant.all_messages,
    socket_channels: state.tenant.socket_channels,
    building_id: state.selection.current_building.building_id,
    landlord_id: state.selection.current_landlord.landlord_id,
	}
}

export default connect(mapStateToProps, {
  addMyMessageToRedux,
})(RadiumHOC)

// ===============================

const comStyles = () => {
	return {
		container: {
      minWidth: '380px',
      maxWidth: '380px',
      height: '600px',
      margin: '0px 0px 20px 0px',
      border: `4px solid ${xMidBlue}`,
      borderRadius: '25px',
      display: 'flex',
      flexDirection: 'column',
      overflow: 'hidden',
      position: 'relative'
		},
	}
}

const headerStyles = () => {
  return {
    header: {
      // height: '50px',
      backgroundColor: xMidBlue,
      textAlign: 'center',
      flex: 1
    },
    recipient_name: {
      color: 'white',
      fontSize: '1.3rem',
      fontWeight: 'bold',
      fontFamily: `'Montserrat', 'sans-serif'`,
      margin: '10px'
    }
  }
}

const chatlogStyles = () => {
  return {
    chatlog: {
      // alignSelf: 'stretch',
      display: 'flex',
      flexDirection: 'column',
      overflowY: 'scroll',
      padding: '15px 15px 20px 15px',
      flex: 7
    },
    messageRow: {
      margin: '5px',
      display: 'flex',
      height: 'auto',
      minHeight: '25px',
    },
    status: {
      height: '20px',
      width: '100%',
    },
    messageMe: {
      display: 'flex',
      backgroundColor: xMidBlue,
      padding: '5px',
      color: 'black',
      maxWidth: '80%',
      textAlign: 'flex-start',
      flexWrap: 'wrap',
      height: 'auto',
      wordWrap: 'break-word',
    },
    messageThem: {
      display: 'flex',
      backgroundColor: 'gray',
      padding: '5px',
      color: 'black',
      maxWidth: '80%',
      textAlign: 'flex-end',
      flexWrap: 'wrap',
      height: 'auto',
      wordWrap: 'break-word',
    }
  }
}

const inputStyles = () => {
  return {
    input: {
      // height: 'auto',
      flex: 1,
      minHeight: '50px',
      minWidth: '380px',
      maxWidth: '380px',
      borderTop: `3px solid ${xMidBlue}`,
      position: 'absolute',
      bottom: '0px',
    },
    inputDiv: {
      position: 'relative',
      height: '100%',
    },
    textInput: {
      minHeight: '50px',
      fontFamily: 'sans-serif',
      fontSize: '14px',
      boxSizing: 'border-box',
      padding: '4px',
      border: '1px solid',
      overflow: 'hidden',
      width: '85%',
      height: '100%',
      position: 'absolute',
      resize: 'none',
      whiteSpace: 'normal',
      padding: '10px 20px 10px 20px',
    },
    textHeight: {
      minHeight: '50px',
      fontFamily: 'sans-serif',
      fontSize: '14px',
      boxSizing: 'border-box',
      padding: '4px',
      border: '1px solid',
      overflow: 'hidden',
      width: '85%',
      visibility: 'hidden',
      whiteSpace: 'pre-wrap',
      wordWrap: 'break-word',
      overflowWrap: 'break-word',
      padding: '10px 20px 10px 20px',
    },
    sendBtn: {
      position: 'absolute',
      height: '20px',
      right: '8px',
      bottom: '8px',
      margin: '5px auto',
      border: '2px gray',
      borderRadius: '5px',
      fontSize: '1rem',
      color: 'gray',
      padding: '5px',
      cursor: 'pointer'
    }
  }
}
