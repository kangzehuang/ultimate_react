import React, { Component } from 'react'
import { connect } from 'react-redux'
import Radium from 'radium'
import PropTypes from 'prop-types'


class Chatline extends Component {

	render() {
		return (
			<div style={comStyles().mainview}>
				<p>Chatline</p>
			</div>
		)
	}
}

Chatline.propTypes = {
}

Chatline.defaultProps = {
}

const RadiumHOC = Radium(Chatline)

function mapStateToProps(state) {
	return {
	}
}

export default connect(mapStateToProps, {})(RadiumHOC)

// ===============================

const comStyles = () => {
	return {
		mainview: {

		}
	}
}
