import React, { Component } from 'react'
import { connect } from 'react-redux'
import Radium from 'radium'
import PropTypes from 'prop-types'

class Dashboard extends Component {

	render() {
		return (
			<div style={comStyles().mainview}>
				"Dashboard"
			</div>
		)
	}
}

Dashboard.propTypes = {

}

const RadiumHOC = Radium(Dashboard)

function mapStateToProps(state){
	return {

	}
}

export default connect(mapStateToProps, {})(RadiumHOC)

// ===============================

const comStyles = () => {
	return {
		mainview: {

		}
	}
}
