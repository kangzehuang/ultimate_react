import React, { Component } from 'react';
import { connect } from 'react-redux';
import Radium from 'radium'
import PropTypes from 'prop-types'
import {
  Link
} from 'react-router'
import { SetupWebsocketMessaging } from '../../api/messaging/websockets_setup'

class LandlordApp extends Component {

  componentWillMount() {
		SetupWebsocketMessaging()
  }

	loadHeader() {
		return (
      <ul>
        <li><Link to='/landlord/dashboard'>Dashboard</Link></li>
        <li><Link to='/landlord/properties'>Properties</Link></li>
        <li><Link to='/landlord/chatline'>Chatline</Link></li>
      </ul>
		)
	}

	render() {
		return (
			<div style={comStyles().app}>
				{this.loadHeader()}
				{this.props.children}
			</div>
		)
	}
}

LandlordApp.propTypes = {
  children: PropTypes.object
}

LandlordApp.defaultProps = {
  children: {}
}

const RadiumHOC = Radium(LandlordApp)

function mapStateToProps(state) {
	return {

	}
}

export default connect(mapStateToProps, {})(RadiumHOC)

// =============================

const comStyles = () => {
	return {
		app: {
			width: '100%',
			height: '100%',
		}
	}
}
